#!/usr/bin/python3
# // ==========================================================================================
# // Script Name: transceiver_Fucntion.py
# // Develop by : Tidaprn Fuasungnoen
# // Release Version : Drafted-00.001
# // Date : 10/04/2023
# // Script Note : New LoRa acceptance code
# // ==========================================================================================
import os, sys
currentdir = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.dirname(os.path.dirname(currentdir)))
from LoRaRF import SX127x
import time
import datetime
from datetime import datetime


# Begin LoRa radio and set NSS, reset, busy, IRQ, txen, and rxen pin with connected Raspberry Pi gpio pins
# IRQ pin not used in this example (set to -1). Set txen and rxen pin to -1 if RF module doesn't have one

busId = 0; csId = 0
resetPin = 27; irqPin = -1; txenPin = -1; rxenPin = -1
LoRa = SX127x()
print("Begin LoRa radio")
if not LoRa.begin(busId, csId, resetPin, irqPin, txenPin, rxenPin) :
    raise Exception("Something wrong, can't begin LoRa radio")

# Set frequency to 433 Mhz
print("Set frequency to 433 Mhz")
LoRa.setFrequency(433000000)

# Set TX power, this function will set PA config with optimal setting for requested TX power
print("Set TX power to +17 dBm")
LoRa.setTxPower(17, LoRa.TX_POWER_PA_BOOST)                     # TX power +17 dBm using PA boost pin

# Configure modulation parameter including spreading factor (SF), bandwidth (BW), and coding rate (CR)
# Receiver must have same SF and BW setting with transmitter to be able to receive LoRa packet
print("Set modulation parameters:\n\tSpreading factor = 7\n\tBandwidth = 125 kHz\n\tCoding rate = 4/5")
LoRa.setSpreadingFactor(7)                                      # LoRa spreading factor: 7
LoRa.setBandwidth(125000)                                       # Bandwidth: 125 kHz
LoRa.setCodeRate(5)                                             # Coding rate: 4/5

# Configure packet parameter including header type, preamble length, payload length, and CRC type
# The explicit packet includes header contain CR, number of byte, and CRC type
# Receiver can receive packet with different CR and packet parameters in explicit header mode
print("Set packet parameters:\n\tExplicit header type\n\tPreamble length = 12\n\tPayload Length = 15\n\tCRC on")
LoRa.setHeaderType(LoRa.HEADER_EXPLICIT)                        # Explicit header mode
LoRa.setPreambleLength(12)                                      # Set preamble length to 12
LoRa.setPayloadLength(15)                                       # Initialize payloadLength to 15
LoRa.setCrcEnable(True)                                         # Set CRC enable

# Set syncronize word for public network (0x34)
print("Set syncronize word to 0x34")
LoRa.setSyncWord(0x34)

def tranmit(Number_Tx, n):
    print("\n-- LoRa Transmitter --\n")
    wh = True
   # Message to transmit
    #k = "HeLoRa World!\0"
    #messageList = list(k)
    #for i in range(len(messageList)) : messageList[i] = ord(messageList[i])
    counter = 0
    #d=0
    # Transmit message continuously
    while counter < Number_Tx :
        
        local = datetime.now()
        message = str(local.strftime("%m/%d/%Y, %H:%M:%S"))+ "\n"+"masssage"+ " <=> " + intp_message +"_"+str(counter)+"\0"
        messageList = list(message)
        for i in range(len(messageList)) : messageList[i] = ord(messageList[i])
        #counter = 0
        # Transmit message and counter
        # write() method must be placed between beginPacket() and endPacket()
        LoRa.beginPacket()
        LoRa.write(messageList, len(messageList))
        # LoRa.write([counter], 1)
        LoRa.endPacket()
        
        # Print message and counter
        # print(f"{message}  times: {d} ")
        print(f"{message}")

            # Wait until modulation process for transmitting packet finish
        LoRa.wait()

        # Print transmit time and data rate
        #print(f"Transmit time: {0:0.2f} ms | Data rate: {1:0.2f} byte/s".format(LoRa.transmitTime(), LoRa.dataRate()))
        print(f"Transmit time: {LoRa.transmitTime():0.2f} ms | Data rate: {LoRa.dataRate():0.2f} byte/s")
        #print(f"Transmit time: {0:0.2f} ms".format(LoRa.dataRate()))

        # Don't load RF module with continous transmit
        time.sleep(3)
        # counter = (counter + 1) % 256
        counter = counter + 1
      

def receive1():
    print("\n-- LoRa Receiver --\n")
    print("Set RX gain to power saving gain")
    LoRa.setRxGain(LoRa.RX_GAIN_POWER_SAVING, LoRa.RX_GAIN_AUTO)

    while True :

        # Request for receiving new LoRa packet
        LoRa.request()
        # Wait for incoming LoRa packet
        LoRa.wait()

        # Put received packet to message and counter variable
        # read() and available() method must be called after request() or listen() method
        message = ""
        # available() method return remaining received payload length and will decrement each read() or get() method called
        while LoRa.available() > 1 :
            message += chr(LoRa.read())
        # counter = LoRa.read()
        #print(f"{LoRa.read()}  {counter}")
        # Print received message and counter in serial
        # print(f"{message}  {counter}")
        print(f"{message}")
        # Print packet/signal status including RSSI, SNR, and signalRSSI
        print("Packet status: RSSI = {0:0.2f} dBm | SNR = {1:0.2f} dB".format(LoRa.packetRssi(), LoRa.snr()))

        # Show received status in case CRC or header error occur
        status = LoRa.status()
        if status == LoRa.STATUS_CRC_ERR : print("CRC error")
        elif status == LoRa.STATUS_HEADER_ERR : print("Packet header error")


mode = int(input("please select LoRa mode : [0] => transmit, [1] => receive: "))
num_ch = True
if mode == 0:
    # now = datetime.today().isoformat()
    while num_ch:
        Number_Tx = int(input("How many numbers to transmit (1 - 256): "))
        if 0 < Number_Tx and Number_Tx <= 256:
            num_ch = False
            intp_message = input("Please, fill in ur message:  ")
            tranmit(Number_Tx, intp_message)
            
        else:
            print("SRATUS FAILED!!, Please: fill it againt")
            
    
elif mode == 1:
    receive1()